{-# OPTIONS_GHC -fno-warn-orphans  #-}
{-# language DataKinds             #-}
{-# language FlexibleInstances     #-}
{-# language MultiParamTypeClasses #-}
{-# language TypeFamilies          #-}
{-# language TypeOperators         #-}
module Generics.SOP.Kind.Examples where

import           Data.PolyKinded.Functor

import           Generics.SOP.Kind
import           Generics.SOP.Kind.Derive.Functor (kfmapDefault)

instance GenericK Maybe (a ':&&: 'LoT0) where
  type CodeK Maybe = '[ '[] ':=>: '[], '[] ':=>: '[Var0] ]

instance KFunctor Maybe '[ 'Co ] (a ':&&: 'LoT0) (b ':&&: 'LoT0) where
  kfmap = kfmapDefault
